# terraform-make

Simple makefile for use with multi-environment terraform configurations

# License

`terraform-make` is licensed under the [MIT License](https://opensource.org/licenses/MIT)
